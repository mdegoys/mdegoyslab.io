# README

## Project description

This is my personal blog. You can visit it at: https://mdegoys.gitlab.io.

## Available Scripts

In the project directory, you can run:

`bundle install` to install the dependencies

`bundle exec jekyll serve` to start the app

## Tools Used

The blog has been generated with [Jekyll](https://jekyllrb.com/).
