---
layout: page
title: A propos
permalink: /about/
---

Ce blog a été fait avec Jekyll. Vous pourrez trouver plus d'information sur [jekyllrb.com](https://jekyllrb.com/)

Vous pourrez trouver le code source pour ce thème Jekyll sur
{% include icon-github.html username="jekyll" %} /
[minima](https://github.com/jekyll/minima)

Vous pourrez trouver le code source pour Jekyll sur
{% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)

Source du favicon: [Twemoji](https://twemoji.twitter.com/), généré par [Favicon.io](https://favicon.io/emoji-favicons/sloth/)
