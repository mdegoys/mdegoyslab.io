---
layout: post
title: "Développement d'un simulateur de notes"
---

Je viens de publier un petit projet en React: ~~https://www.simulateur-notes.fr~~ (édition du 17 août 2021: renommé depuis [https://www.simulateur-bac.fr](https://www.simulateur-bac.fr)). C'est un projet que je souhaitais faire depuis un moment car c'est type d'outil très populaire sur internet, et ça collait bien avec mon autre gros projet d'aide aux lycéens en français et philosophie: [https://www.20aubac.fr](https://www.20aubac.fr). J'ai mis au final deux mois pour le finaliser, ce qui peut sembler beaucoup pour un outil qui parait aussi simple, mais comme toujours avec le développement, il est très difficile de se rendre compte de la durée de travail nécessaire. Il est donc temps de faire le bilan sur ce développement: les difficultés rencontrées, les bonnes surprises, et enfin ce que j'ai pu en retirer.

## Les difficultés rencontrées

### Une performance qui pouvait poser question

Je n'ai pas eu à proprement parler de grosses difficultés sur le coeur de l'application. J'ai pu décider assez vite de la structure de l'application, avec d'un côté un objet JSON qui contient toutes les données: matières, épreuves, coefficients et notes associées, et de l'autre un algorithme qui calcule la moyenne de l'utilisateur à chaque changement. Pour avoir fait plusieurs applications React déjà lors de ma formation initiale chez Simplon, je savais à peu près comment j'allais pouvoir m'y prendre. J'ai juste eu doute sur le fait de tout recalculer à chaque changement, pour de questions de performances, mais au final je n'ai jamais rencontré de soucis apparent.

### Les tests d'intégration, difficile à faire fonctionner en Intégration Continue

Par contre j'ai eu plus de difficultés avec l'intégration continue. Je voulais en effet avoir des tests d'intégration, et j'ai commencé à utiliser pour cela [TestCafé](https://devexpress.github.io/testcafe/). Tout fonctionnait bien, mes premiers tests passaient et j'utilisais même une librairie qui permettait se sélectionner directement les composants React. Malheureusement, les choses se sont gâtées lorsque j'ai voulu mettre en place l'intégration continue (ou CI) avec GitLab. J'y ai passé beaucoup de temps, pour au final ne pas arriver à faire tourner les tests sans erreurs. Je me suis finalement rabattu sur [Cypress](https://www.cypress.io/), un autre framework de tests d'intégration. C'était de la même manière pas évident à mettre en place, mais j'y suis arrivé. Le peu de documentation/personnes qui ont rencontré les mêmes difficultés, font que ça rendait chaque erreur plus difficile à résoudre.

### TypeScript, et ses erreurs parfois mystiques

L'autre grosse difficulté concerne TypeScript. J'ai implémenté ce langage sur le tard, et sans vraiment de connaissance dessus. TypeScript lève des erreurs (c'est son rôle en même temps :)) pour des erreurs de types non précisés ou non cohérents avec le reste du code. La difficulté avec ces erreurs est qu'elles ne sont pas toujours facile à comprendre et donc à corriger. J'ai par exemple mis beaucoup de temps pour comprendre que je ne pouvais pas déclarer un type avec des valeurs stockées en variable. Je pensais que l'erreur venait de ma variable elle-même, comme TypeScript semblait l'expliquer, alors que ce n'était pas le cas. La bonne nouvelle par contre est de lever des erreurs de types qui sont cachées, comme dans mon cas le fait de comparer sans s'en rendre compte un nombre et une variable, que JavaScript accepter de faire sans broncher (`"3" >= 2` est évalué à `true`). L'autre bonne nouvelle est que TypeScript est quand même assez flexible pour accepter des arguments de type `any`, lorsque l'implémentation devient trop chronophage pour un bénéfice pas évident (évidemment ça reste de la triche :)).

![Une erreur TypeScript compliquée](/assets/typescript_oh_boy.png) *Peut-être l'occasion de mettre `any`, histoire d'être "tranquille" :)*

## Les bonnes surprises

### Les tests unitaires, qui permettent de faire du bô code

J'ai toujours été convaincu de la nécessité de faire du code couvert par des tests. Ce projet en est encore une preuve pour moi. En effet à l'occasion de la réécriture du code, j'ai pu faire pas mal de modification sur l'algorithme de calcul, le coeur de l'application (si celui ci est faux, l'application perd tout son intérêt). Il est alors très facile d'introduire un changement de comportement sans s'en rendre compte, même en refaisant régulièrement des tests manuels. Cela m'est arrivé en changeant une variable lors du passage à TypeScript. J'ai alors saisi comme note minimale la valeur "1", sans vraiment y réfléchir, alors qu'on peut très bien avoir une note de 0. La conséquence était qu'une note de 0 ou 0.5 était dès lors considérée comme non valide. Les tests manuels ne laissaient rien paraitre, et heureusement j'avais un test sur le cas d'un note très basse, qui m'a du coup alerté. Bref, vive les tests unitaires !

### TailwindCSS, et le "dark mode"

Je suis tombé assez fan de [TailwindCSS](https://tailwindcss.com/) depuis que je l'utilise pour 20aubac. Je l'ai donc réutilisé pour ce projet, en voulant cette fois implémenter une possibiité qu'ils offrent avec la version 2: le "dark mode" ou mode sombre. TailwindCSS offre un moyen très simple et très efficace de le mettre en place, en utilisant dans ses classes le préfixe `dark:`, suivi de la modification qui doit s'opérer lors de l'application du mode sombre. J'aime beaucoup les modes sombres, qui sont généralement agréables à lire et relaxants pour les yeux. La difficulté est de rendre leur implémentation simple, et TailwindCSS le fait avec brio !

### GitLab CI, flexible et pratique 

Lors de mes difficultés avec la mise en place des tests d'intégrations, j'ai eu l'occasion d'en apprendre plus sur GitLab CI et les riches possibités qu'il offre pour un outil proposé gratuitement. C'est simple: on peut faire beaucoup de choses ! A commencer par définir des étapes (ou "steps" en anglais) et ainsi obtenir de jolis schémas sur l'avancée de l'intégration. Et on peut en définir beaucoup des étapes: faire un "build", faire tourner les tests unitaires, puis faire tourner les tests d'intégration, puis le "linter", puis le test de couverture des tests, puis enfin publier l'application... Tout cela en une commande (`git push`), et ce sans même payer l'hébergement dans le cas de GitLab Pages ! 

![Les étapes de GitLab CI](/assets/gitlab_steps.png) *Les étapes de GitLab en action - depuis [un article](https://medium.com/@mutebg/using-gitlab-to-build-test-and-deploy-modern-front-end-applications-bc940501a1f6) par ailleurs très intéressant*


## Ce que j'ai appris

### Les tests d'intégration, c'est long !

Les tests d'intégration sont très rassurants, en se plaçant du point de vue de l'utilisateur et en vérifiant les cas de bases de l'application. Par contre ils sont très longs à exécuter en intégration continue, avec dans mon cas l'installation du image Docker qui prend beaucoup de temps (en local cela reste rapide). Au final chaque déploiement est rallongé d'au moins 5 bonnes minutes, pour tester des cas qui restent somme toute basiques. Néanmoins ces tests restent à mes yeux essentiels, et je pense aussi les utiliser pour mon projet principal. Je reste d'avis que la rapidité de déploiement passe après l'exécution sans heurt des cas d'usages essentiels pour les utilisateurs de l'application.

### Pas facile le SSR en React

Je me suis également penché sur le déploiement de l'application en SSR (Server Side Rendering), pour des questions de référencement. J'ai dû abandonner l'idée car les solutions proposées par React dans sa doc sont soit de pré-"builder" son app avec des solutions comme react-snapshots, soit d'avoir son serveur Node qui génère l'app avant de l'envoyer au client. La première solution, beaucoup plus simple, était intéressante dans mon cas, mais je n'ai jamais réussi à la faire fonctionner. Aussi le projet n'a pas été maintenu depuis longtemps. Pour ce qui est d'avoir son propre serveur, cela remettait en cause toute l'architecture que j'avais mis du temps à mettre en place. Surtout, je me suis rendu compte que les outils d'audit en référencement arrivaient à lire mon contenu sans SSR. Au final, malgré tout le buzz autour de ce concept, cela ne semble pas être la priorité pour l'équipe derrière React, et peut-être que ça n'a pas besoin de l'être ?

### Bing, très gentil avec les nouveaux sites, pas comme Google

J'ai soumis le site à Bing et Google le même jour. J'ai été surpris de la vitesse à laquelle les deux moteurs ont référencé celui-ci, puisque dès le lendemain je pouvais le trouver listé. Par contre la différence de traitement entre les deux moteurs pour les nouveaux entrants est très significative. En effet sur les mots-clés "simulateur notes", je me suis dès le départ mis en première place par Bing. Le nom de domaine, spécifiquement composée des deux mots-clés, a dû évidemment aider. Par contre, chez Google, point de salut. Cela ne m'étonne guère car je sais que Google met systèmatiquement de côté les domaines à l'historique vierge. Il est quand même intéressant de voir que les deux moteurs, qui ont à priori des fonctionnements assez similaires, appliquent ici une règle assez différente !

![Bin première place](/assets/bing_premiere_place.png) *Bing semble très bien considérer les mots-clés contenus dans le domaine*

## Conclusion

Au final, le développement de ce simulateur de notes a été plutôt amusant. React est un très bon outil pour ce type de petites applications, et le rendu est je trouve plutôt satisfaisant. Je n'ai pas eu de grosses difficultés critiques, même si la mise en place des tests d'intégrations sous GitLab CI m'a donné du fil à retordre. GitLab CI n'est pas vraiment en tort, qui reste par ailleurs un très bon outil pour publier petit site seulement côté client. Je comprends désormais pourquoi les tests d'intégrations sont peu utilisés: compliqués à mettre en place et longs à faire tourner à chaque déploiement. Les tests unitaires eux, tout en étant toujours rapides, m'ont permis d'éviter le pire: une erreur introduite en dernière minute à l'occasion d'une réécriture de code. Bref même en restant sur des solutions techniques simples et standards, on peut arriver à des résultats satisfaisants. A voir maintenant comment une application client sans SSR se débrouille en référencement avec le temps !

