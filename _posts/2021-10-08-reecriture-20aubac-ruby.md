---
layout: post
title: "Réécriture de 20aubac en Ruby"
---

Je viens de finir de redévelopper un site que je mantiens depuis longtemps, [20aubac](https://www.20aubac.fr), et qui était auparavant en PHP sans framework. Le fait d'avoir appris Ruby et le framework Ruby on Rails m'a donné envie de m'entrainer à mieux comprendre ce langage et son framework associé, en développant un projet de zéro. Et quoi de mieux de partir sur un vrai projet, 20aubac étant visité par plusieurs milliers de visiteurs chaque jour, et donc ayant sa (modeste) utilité ?

Comme d'habitude ce projet m'a pris beaucoup plus de temps que prévu, et de quelques mois de temps planifié, cela s'est transformé à environ un an de développement en réalité. Et au bout de tout ce temps j'en ai tiré quelques enseignements, des difficultés rencontrées à des découvertes salutaires.

## Les difficultés rencontrées

### Les associations polymorphiques

L'avantage de Rails, c'est de pouvoir se reposer sur les méthodes proposées, et d'en profiter pour mieux gérer son code et ses données. Dans mon ancienne application, en PHP, toutes mes requêtes à la base de données étaient écrites en SQL. La conséquence était que ma base de données, pour me simplifier la vie, n'était constituée que de quelques tables, afin d'éviter de faire trop de jointures et de trop complexifier les requêtes réalisées. Avec Rails, l'existence d'Active Record et des relations has_one, has_many, belongs_to permet de largement simplifier ces problèmes et de s'autoriser à avoir des tables séparées lorsque cela s'y prête. Dans mon cas cela me permettait de séparer les ressources internes et externes, qui avaient chacune leurs champs dédiées, et d'éviter ainsi des tables à "trous".

Mais un problème s'est posé quand j'ai voulu implémenter des requêtes de classement sur les deux tables, qui avaient aussi des données communes, et mes requêtes, même en Active Record, devenaient trop compliquées. La solution a alors été d'utiliser les associations polymorphiques, avec un table supplémentaire, qui reprenait les données communes, en plus des deux sous-tables, qui comprenaient leurs données propres. Sur le papier cela fonctionne très bien, mais la mise en place fut laborieuse, notamment pour faire fonctionner les formulaires qui devaient créer trois enregistrements différents, donc pour trois modèles différents, en une validation. Rails m'a notamment posé des soucis de compatibilité entre la méthode accepts_nested_attributes_for, qui permet cela, et les associations polymorphiques. J'ai pu au final trouver sur Stack Overflow une bidouille qui consiste à réecrire une méthode interne à Rails dans mon modèle et de résoudre ce problème, mais ce type d'arrangement n'est pas forcément une bonne nouvelle.

### Test des webhooks

Un autre souci rencontré fut l'utilisation des webhooks, utilisés dans mon cas pour confirmer les paiements réalisés. Dans le principe cela fonctionne bien car c'est du traitement asynchrone, et en cas d'erreur le webhook va réessayer de faire passer l'information de paiement. 

Mais qui dit paiement, dit cas d'usage sensibles, pour lesquels il faut tester le maximum de cas d'erreurs possibles dès le départ. Aussi les webhooks, pour peu qu'ils ne proposent pas d'environnement de test, ne peuvent être testés qu'en environnement de production. Et pour peu que la documentation ne soit pas très claire, il faut bidouiller pour récupérer les informations transmises au webhook. Enfin pour l'écriture des tests automatisés, le manque d'information sur ce qui être transmis au webhook fait que je n'ai testé que peu de cas. Le risque étant en cas d'erreurs non prévues, il faille réagir vite pour gérer au mieux celles-ci.

### Des requêtes performantes ?

Une des premières choses que j'ai appris sur Rails c'est le problème des requêtes n+1. Et c'est vrai qu'on en fait très vite, même en étant prévenu. Mais même sans faire 'n+1' requêtes à chaque chargement de page, Active Record, qui facilite bien il est vrai la vie, masque de fait le nombre de requêtes SQL qu'il génère. Surtout, si l'on a beaucoup de conditions et de jointures à appliquer, réduire leur nombre tient parfois du casse-tête. Mais si on y arrive, c'est pour autant très bénéfique, lorsqu'on multiplie chaque chargement de page par le nombre de visiteurs, et chaque visiteur par le nombre de pages consultées.

C'est là où j'ai dû me réduire à créer des champs dédiés, pour pré-calculer des résultats et éviter de regénérer les mêmes requêtes indéfiniment. Cela créé bien sûr un peu plus de complexité dans l'application, mais les performances me semblent prioritaires à la simplicité du code. C'est par ailleurs là une de mes difficultés: me rendre compte de l'impact du nombre de requêtes sur les performances de l'application.


## Les bonnes surprises

### Les gems, j'aime

Développer sous Ruby on Rails présente également l'avantage des gems, que l'on peut retrouver sur [RubyGems.org](https://rubygems.org/). Cela évite de développer des choses qui sont déjà développées par d'autres, et qui ne sont pas au centre de son application, tout en étant indispensables. L'exemple parfait est l'authentifcation, qui n'apporte rien en tant que tel à l'utilisateur, et qui pourtant est une partie critique, car sur elle repose toutes les fonctionnalités qui demandent de connaitre l'auteur de l'action. Une grande partie des applications ont besoin d'un système d'authentifcation, et y ont donc toutes un intérêt partagé, d'où l'existence des gems.

J'utilise ainsi des gems qui me facilitent largement la vie, comme [Devise](https://github.com/heartcombo/devise) pour l'authentifcation, [Kaminari](https://github.com/kaminari/kaminari) pour la pagination, [pg_search](https://github.com/Casecommons/pg_search) pour le moteur interne de recherche, ou encore [sitemap_generator](https://github.com/kjvarga/sitemap_generator) pour générer des sitemap au bon format. Ce sont toutes des fonctionnalités que j'ai codé par moi-même auparavant en PHP, mais certainement beaucoup moins bien, avec sans doute des failles de sécurité, des bugs, et enfin des possibilités moins étendues. Tout coder par soi-même présent l'avantage d'avoir exactement le résultat souhaité, mais demande de passer du temps sur des problèmes qui ne donnent que très marginalement de la valeur à son application.

### Les API, j'aime aussi

Une autre chose que j'ai pu utiliser, et dont je n'avais pas du tout l'usage auparavant, sont les API. Je pensais pas qu'elles pouvaient se révéler utiles dans mon cas, car toute mes données sont en locales, et je n'ai pas vraiment d'informations complémentaires à présenter. Cependant j'ai eu deux besoin qui m'ont fait utiliser des API, et qui dans ces cas précis m'apportent beaucoup:
- la génération de PDF à partir de pages HTML, qui me permettent de sauvegarder une archive des liens externes proposés et qui peuvent disparaitre
- la récupération des résultats donnés par Google pour des termes données, qui me permettent de savoir si je peux rendre un contenu payant ou non

Dans ces deux cas, les API me donnent des données ou informations très utiles, et le fait de payer pour celles-ci est même plutôt rassurant pour la qualité des informations données, tout comme la maintenance du service rendu. Ces deux API sont d'ailleurs proposées par la même société, [apilayer](https://apilayer.com/), qui propose en plus des volumes gratuits intéressants, et des tarifs honnêtes, tout cela sans engagement.

### Les services tiers, ça gère

Enfin, après les gems et les API, Ruby et son écosystème est compatible avec des services tiers, qui là encore apportent leur confort. En PHP par exemple les emails étaient envoyés par le serveur qui hébergeaient également les fichiers. Ce n'est pas le cas avec mon serveur actuel. De même pour l'upload de documents, que je ne peux plus ajouter directement sur mon serveur, mon hébergeur Scalingo, à la manière d'Heroku, ne faisant pas persister les fichiers d'un reboot du serveur à un autre. Mais ce qui peut paraitre être un défaut est plutôt un avantage, car ce sont des tâches qu'il vaut mieux déléguer là encore à des services qui le font très bien, et qui offrent des services en plus.

Pour les emails j'ai ainsi découvert les services de [Mailjet](https://fr.mailjet.com), dont l'installation est très rapide sous Rails (grâce à leur très bonne doc), et qui permet d'avoir un suivi des emails envoyés. Et pour le stockage des fichiers, Rails offre la fonctionnalité [Active Storage](https://edgeguides.rubyonrails.org/active_storage_overview.html), qui permet d'uploader très facilement des fichiers sur des services externes, qui offrent là encore un suivi des fichiers hébergés. Bien sûr ces services ont un coût, mais c'est aux volumes des emails envoyés ou des données hébergées, donc pour des petites applications cela reste intéressant. Il reste tout de même à surveiller l'évolution de ces coûts.

## Ce que j'ai appris

### Penser d'abord, et ne pas toujours faire ensuite

Une chose que j'ai apprise avec l'expérience, c'est d'éviter de foncer tête baissée dans le développement, sans s'être assuré auparavant de son utilité. Je m'en rends compte d'autant plus en réécrivant le code de 20aubac, où j'ai mis de côté plein de fonctionnalités, sans être réellement certains qu'elles aient leur utilité. Un exemple parmi d'autres: le formulaire d'envoi d'email. C'est quelque chose que l'on retrouve souvent sur les sites web, et j'en avais un moi-même auparavant. Mais aussi simple que cela puisse paraître, cela demande un paramètrage pour l'envoi en lui-même, la vérification des champs, des mesures contre le spam... Tout ça pour me rendre compte que je ne recevais que très peu d'emails, et parfois même plutôt des emails directement écrits à mon adresse. Autant finalement ne pas avoir de formulaire du tout et de simplement laisser son adresse email !

### RSpec, ça finit toujours par passer

Rails est également pensé pour les tests automatisés, qui deviennent indispensables dès que l'on veut faire du développement de manière professionnelle. C'est d'ailleurs quelque chose qui me manquait beaucoup lors de mes développements précédents en PHP, où je devais tout tester manuellement pour être sûr du fonctionnement de mon site, sans pouvoir en plus toujours tout tester. [RSpec](https://rspec.info/) est à ce titre un framework de test, qui offre plein de méthodes pour ce faire, et qui est largement reconnu dans la communauté Rails.

Surtout, on peut tester beaucoup de choses, ce qui est très agréable: partir réellement de son cas d'usage pour le coder en test. Je ne teste pas tout, mais toutes les méthodes au coeur de l'application sont couvertes, et si un jour je me rends compte qu'il me manque des tests, je serais confiant pour rajouter des tests avec RSpec. Car en effet, même s'il y a des aspects avec lesquels j'ai encore du mal (les stubs, mocks, doubles, les before allow...), je sais qu'à priori je retomberai toujours sur même pieds pour tester le comportement attendu.

### Se passer des GAFAMs, réalisable mais compliqué

Une des choses que j'ai essayé d'appliquer a été d'éviter autant que possible l'utilisation des services des GAFAMs. J'ai un peu réussi, mais pas complètement, même si je n'abandonne pas l'idée. Sur le web notamment, il est très difficile d'éviter Google. Pour les statistiques de consultation Google Analytics est ainsi utilisé par de nombreux sites, mais il existe heureusemnt quelques alternatives. Pour ma part j'utilise [Xiti](https://www.xiti.com/), qui est gratuit dans sa version de base, même s'il n'offre que peu de données à exploiter. Pour les publicités, j'ai mis du temps à trouver une alternative à Google Adsense, qui a l'avantage d'accepter tous les petits sites. J'ai fini par trouver [TheMoneytizer](https://www.themoneytizer.com/), par du bouche-à-oreille, et qui présente quelques avantages comme le suivi de compte en français, la facilité de mise en place et les formats originaux proposés. 

J'ai par contre dû me résoudre pour l'instant à utiliser AWS pour faire tourner Active Storage, par facilité et par gain de temps, mais je ne désespère pas à terme d'arriver à le remplacer par un OVHCloud par exemple, même si cela doit prendre un peu de temps, de recherches et d'essais. J'ai pu par contre éviter de passer par Heroku, lui aussi basé sur AWS, pour utiliser [Scalingo](https://scalingo.com/fr), un équivalent français qui offre un très bon support et un bonne documentation, et qui héberge les données sur les serveurs de 3DS, autrement dit le français Dassault Systèmes.

Bref la facilité conduit à utiliser les services des GAFAMs, car ils sont très visibles, offrent une bonne documentation et la plupart des tutos sont basés sur leur utilisation. Mais la concurrence existe toujours, et on peut faire des bonnes découvertes, pour peu que l'on se donne un peu la peine d'explorer :).

## Conclusion

En conclusion, je dirais que j'ai eu beaucoup de plaisir à développer sous Ruby on Rails. C'est un environnement très robuste et très bien pensé pour développer de zéro une application. Le framework présente de très gros avantages pour bien répartir son code avec l'architecture MVC (Modèle, Vue, Contrôleur), le tester avec RSpec ou encore faire des requêtes à la base de données avec Active Record. Surtout, on peut utiliser beaucoup d'autres codes, sous la forme de gems, d'API ou de services tiers, pour se concentrer d'abord sur la valeur ajoutée de son application.

Ce temps précieux que l'on gagne peut être du coup à s'interroger sur les performances de son application, sur la gestion des erreurs ou encore sur les tests à implémener ou non. Et je n'ai que peu de doute sur l'avenir, si j'ai besoin de rajouter des fonctionnalités, ou d'améliorer celles existantes, grâces aux tests automatisés et grâce à l'architecture du code, plus simple à maintenir. Ça amène paradoxalement à moins coder, car on peut avoir une approche plus modulaire, en fonction de ce qui est utile ou non, et de se dire qu'on peut facilement implémeter de nouvelles fonctionnalités, sans tout reprendre, voire tout casser !
