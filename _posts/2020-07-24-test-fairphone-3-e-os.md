---
layout: post
title: "Test du Fairphone 3 avec /e/ OS"
---

J'ai acquis il y a près d'un mois maintenant un Fairphone 3, que lequel j'ai installé l'OS alternatif /e/ OS. Je voulais en effet sortir du duopole Apple/Google, et ne plus avoir à choisir entre l'achat de téléphones très chers et peu innovants chez Apple, ou moins chers et très perméables en termes de données privées chez Google. Voici mon bilan après 3 semaines d'utilisation.

![Le Fairphone 3 avec /e/ OS (image tirée du site de la e foundation)](/assets/fairphone-3.png)

### Le Fairphone 3

#### Un téléphone éthique et durable, en rupture avec l'industrie

Le [Fairphone 3](https://www.fairphone.com) est un téléphone qui se veut éthique, car offrant de bonnes conditions aux personnes travaillant à l'extraction des minéraux rares servant à sa fabrication. Mais aussi parce qu'il est durable: Fairphone, la marque, ne sort qu'un modèle tous les 3/4 ans, lorsque l'industrie renouvelle ses gammes tous les 6/12 mois. Pour en assurer la durabilité, il est possible d'acheter sur le site du constructeur des composants comme la batterie ou l'écran, mais aussi des composants plus internes comme le module photo. À l'usage le Fairphone 3 semble très solide avec son revêtement plastique, et se distingue ainsi d'un iPhone avec son verre que l'on craint de rayer dès qu'on le met dans la proche. Vous ne posséderez pas un Fairphone comme marqueur social ou pour avoir le dernier gagdet à la mode, mais plutôt pour avoir un smartphone sur le long-terme, que vous n'aurez pas peur d'abîmer ou d'user du fait de sa batterie facilement remplaçable. Sur le point de la durabilité, le Fairphone semble remplir parfaitement la tâche.

#### Un téléphone aux performances moyennes

Qu'en est-il des performances ? Pour 450€, le prix du Fairphone 3, on trouve des très bons téléphones milieu/début du haut de gamme qui ont de très bons écrans/puissance/appareils photos. Mais le Fairphone 3 n'est pas un téléphone haut de gamme. En fait tous les composants qu'il offre sont moyens: le processeur ne conviendra pas aux jeux gourmands (pour exemple le jeu Clash Royal ralentit trop lorsqu'il y a trop d'unités, le rendant injouable), l'appareil photo offre des rendus  corrects sans plus, le son est imparfait, et l'écran plutôt banal. Le Fairphone est par contre réactif lorsqu'il s'agit de l'allumer, de naviguer dans les menus et différentes applications. Le GPS et le lecteur d'empreinte sont également efficaces lorsque sollicités. Bref il n'excelle dans rien, mais fait son travail correctement. À voir cependant comment il se comporte dans quelques années, lorsque les processeurs et puissance des autres téléphones auront encore fait un bond.

#### Ouvert dans l'esprit, à la bidouille et aux OS alternatifs

Là où le Fairphone se révèle intéressant, outre sa durabilité, c'est lorsqu'il s'agit d'y installer un OS alternatif. Le Fairphone embarque de base Android et toutes les applications et services Google qui vont avec. Mais il possible et facile d'y installer un OS "non officiel" comme /e/ OS, qui est une sorte d'Android sans Google (et donc sans les traqueurs publicitaires qu'il embarque). Surtout, Fairphone a annoncé officiellement travailler avec la e foundation, ce qui permet d'assurer un support sur le long terme de ses smartphones. C'est un point rassurant, quand on sait que le support des téléphones par les OS alternatifs dépend du bon vouloir des développeurs et de la communauté. Aussi dans les faits, l'installation d'/e/ OS sur le FP3 est plutôt simple et rapide, grâce notamment à un guide en ligne qui détaille toutes les étapes à suivre. 

Au final le Fairphone 3 est un téléphone durable et solide, moyen dans ses performances, mais opérationnel pour toutes les tâches quotidiennes. Surtout, c'est un téléphone ouvert aux OS alternatifs, comme /e/ OS, dont nous allons parler dès à présent.

### /e/ OS

#### L'OS alternatif qui se veut à la portée de tous

[/e/ OS](https://e.foundation/) est pensé pour les geeks, mais également pour les personnes non techniques, qui veulent s'assurer du respect de leur vie privée. À ce titre les utilisateurs sont pris par la main, et ce dès l'achat, avec des téléphones clés en main proposés sur le site de la e foundation. Au premier démarrage du téléphone, toutes les apps "de bases" sont présentes: navigateur, email, cartographie, musique, etc. Un store d'applications est proposé avec de nombreuses autres apps connues du grand public. La sauvegarde des données est également prévue avec la création d'un compte /e/, basé sur la très bonne solution Nextcloud. Finalement, on le prend très vite en main, et ce sans forcément se rendre compte que le téléphone n'embarque aucune dépendence aux services Google, et c'est déjà un petit tour de force.

#### Un OS encore en bêta, avec ses bugs et ses rigidités

Tout n'est pas encore finalisé, à commencer par de nombreux petit bugs de crash de microG (qui remplacent les services Google, pour le bon fonctionnement des apps), ou même d'apps simple comme celle de prise de notes, mais qui n'empêchent pas l'utilisation du téléphone. Aussi les raccourcis ou l'écran d'accueil ne sont pas personnalisables, mais ce ne m'a pas paru vraiment gênant pour une beta. Plus gênant par contre est l'apport relatif du store d'applications propre à /e/ OS. Celui-ci n'est pas très clair sur les apps ou non présentes, et n'est ni aussi complet qu'un Aurora Store (où sont présent toutes les apps du Play Store), ni aussi intéressant qu'un F-Droid (qui ne propose que des apps open source, dont [certaines pépites](https://mdegoys.gitlab.io/2020/07/17/3-app-f-droid-sympas.html)). Ce point est d'autant plus important que les apps font toute la valeur d'un écosystème, et de mon point de vue la e foundation devrait donner plus d'attention à son store, la présentation et la sélection des apps (comme par exemple, pour chaque app connue, donner des alternatives respectueuses de la vie privé).

#### Un OS ambitieux, mais avec des perspectives encore floues

/e/ OS, dans son email de bienvenue, ambitionne de devenir le troisième OS de référence derrière iOS et Android. A la différence d'Apple et de Google, e foundation est un organisme non lucratif, ce qui semble être la bonne voie pour poser les fondements d'un troisième OS. Aussi, plutôt que de réinventer la roue, /e/ OS rassemble tous les projets open source les plus prometteurs comme LineageOS, MicroG, Nextcloud ou Chromium pour son navigateur. Et c'est bien l'intégration de tous ces projets qui fait aujourd'hui la valeur ajoutée d'/e/ OS. Mais au delà de la première réussite d'un OS respectueux de la vie privée et fonctionnel, se pose des questions sur son avenir: être un simple "assemblage" de projets open source est-il suffisant pour marcher sur les plates-bandes des Gafam ? Au delà du respect de la vie privée de ses utilisateurs, quelle valeur ajoutée est offerte par cet OS ? Aussi, est-ce que le seul appel à des dons peut suffire à le financer et le rendre viable sur le long terme ?

### Conclusion

Au final, le Fairphone 3 associé à /e/ OS est une option viable pour remplacer un iPhone ou un Android Google, ce qui est déjà une très bonne nouvelle. A voir comment l'absence de puissance du Fairphone 3 pèse avec le temps, surtout pour un smartphone qui est censé tenir sur au moins 4 à 5 ans. Pour ce qui est d'/e/ OS, l'essentiel fonctionne, mais il manque encore des corrections et des options de personnalisation. Surtout, la vision à long-terme m'interroge,  que cela soit sur le financement ou sur la valeur ajoutée offerte. Dans tous les cas, le monde du smartphone a cruellement besoin d'un OS libéré des vélleités hégémoniques des Gafam, et la proposition d'un Android expurgé des services Google, tout en profitant de son écosystème d'applications, semble être une voie très prometteuse.
