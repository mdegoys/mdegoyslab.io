---
layout: post
title:  "Mon premier post avec Jekyll !"
---

Je viens d'ouvrir ce blog avec Jekyll, qui est basé sur Ruby, mon langage de prédilection <3. Je ne connais pas bien le markdown et donc les possibilités en terme de présentation, mais je vais tâcher de comprendre comment ça marche pour pouvoir égayer un peu mes écrits.

Je vois qu'il n'y a pas trop d'options pour commenter les billets, ce qui n'est pas étonnant vu le principe de Jekyll qui fonctionne qu'avec des pages statiques, donc sans bases de données. Vous pouvez du coup m'écrire par email, ou par/sur mastodon (les liens sont situés en pied de page).
