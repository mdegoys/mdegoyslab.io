---
layout: post
title:  "3 apps sur F-Droid à découvrir"
---

Je suis en train de découvrir les joies d'un Android "dé-googlisés", et qui dit absence du Play Store, dit utilisation d'un store alernatif comme [F-Droid](https://f-droid.org/fr/). Mais j'étais  au départ assez sceptique sur la qualité des apps pouvant y être proposées. En effet je partais du principe que des apps gratuites et sans pubs (et donc non rémunérées, en dehors des éventuelles donations) sont forcément moins bien que des apps commerciales. Les apps de Play Store ont également pour avantage d'être très utilisées et donc de recevoir beaucoup de retours utilisateurs, permettant leur amélioration continue. Au final, j'ai été supris par la qualité des apps proposées sur F-Droid, en plus de la satisfaction de savoir qu'elles n'incluent ni pubs, ni ciblage publicitaire.

Voici en exemple trois apps (en fait quatre, deux du même auteur se ressemblant beaucoup) qui me semblent très intéressantes.

### NewPipe

[NewPipe](https://f-droid.org/packages/org.schabi.newpipe/) est sans doute l'une des applications les plus connues sur F-Droid, qui est d'ailleurs exclusive à ce store alternatif. Et pour cause, cette app ce visionnage de vidéo permet notamment trois choses qui vont à l'encontre du modèle économique de Google/YouTube: de ne plus voir les pubs, de télécharger les vidéos, mais surtout ce que je trouve le plus intéressant et inédit, elle offre une fonction d'"arrière-plan". Cette fonctionnalité permet d'écouter la vidéo sans devoir rester dessus, ni garder son téléphone déverouillé. Cela est très plaisant pour écouter des vidéos en mode podcast, voire pour écouter de la musique, comme avec Spotify, mais gratuitement.

### EscapePod/Transistor

En parlant de podcasts, j'étais auparavant très satisfait de l'application proposées par défaut sur l'iPhone, et j'avais toujours été déçu sur cet usage par les apps Android. F-Droid m'a alors fait découvrir cette app sans prétention, très minimaliste, mais qui fait parfaitement le travail: [EscapePod](https://f-droid.org/en/packages/org.y20k.escapepod/). Une barre de recherche pour ajouter un podcast, un glissement de doigt pour actualiser les flux, et un bouton pour télécharger/écouter les épisodes. Difficile de faire plus simple :). Il faut juste connaitre à l'avance les podcasts souhaités (du moins leur nom), car il n'y a pas d'espace de découverte de nouveaux programmes. J'ai par contre un souci avec un podcast particulier que je n'arrive pas à télécharger, mais le développeur de l'app semble particulièrement réactif aux demandes, donc je ne doute pas que je vais pouvoir trouver une solution.

Du même développeur, [Transistor](https://f-droid.org/en/packages/org.y20k.transistor/) permet cette fois-ci d'écouter la radio. Le principe et la présentation sont les mêmes, sauf qu'on remplace les flux de bandes sonores pré-enregistrées par des flux audio en direct. Simple et terriblement efficace - je n'avais jamais trouvé par exemple d'équivalence sur iPhone.

### OTP Authenticator

[OTP Authenticator](https://f-droid.org/fr/packages/net.bierbaumer.otp_authenticator/) est un petit client, là aussi très minimaliste, pour les authentifications en deux étapes (2FA). L'app est extrêmement simple et donc très efficace, puisqu'elle demande un minimum de configuration et de navigation. Je ne l'utiliserai par contre pas sur le long terme, car tous mes comptes avec double authentification sont déjà gérés par Authy (développée par Twilio), qui elle sauve la configuration dans le cloud. Mais Authy demande plus de manipulation avant d'arriver au code nécessaire pour s'authentifier, ce qui me fera regretter ce petit utilitaire très pratique.<br /><br />

A noter qu'en dehors de NewPipe, les trois apps ici présentées sont également disponibles sur le Play Store. Ce qui rendrait l'utilisation de F-Droid inutile... à trois choses près.  Premièrement sur le Play Store ces apps sont noyées dans une multitude d'autres apps commerciales et sont de fait peu visibles, surtout sur des recherches standard de type "podcasts" ou "radio". Ensuite le Play Store ne donne aucune indication sur la présence de pubs ou de ciblage publicitaire. Enfin le Play Store demande lui d'être authentifié et donc d'accepter de donner encore plus d'informations à notre sujet à Google.


